//
//  Track.swift
//  SoundCloudApp
//
//  Created by Nataly on 30.09.2021.
//

import Foundation

struct TrackList: Decodable {
    let collection: [Track]?
}

struct Track: Decodable {
    let title: String?
    let labelName: String?
    let image: String?
    let user: User?
    let userFavorite: Bool?
    let id: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case labelName = "label_name"
        case image = "artwork_url"
        case user = "user"
        case userFavorite = "user_favorite"
        case id = "id"
    }
}

struct User: Decodable {
    let username: String?
}
