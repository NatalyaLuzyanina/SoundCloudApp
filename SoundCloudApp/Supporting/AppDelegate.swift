//
//  AppDelegate.swift
//  SoundCloudApp
//
//  Created by Nataly on 21.09.2021.
//

import UIKit
import KeychainSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let accessToken = KeychainSwift().get("accessToken")
        if accessToken != nil {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homePage = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController
            self.window?.rootViewController = homePage
            self.window?.makeKeyAndVisible()
        }
        return true
    }



}

