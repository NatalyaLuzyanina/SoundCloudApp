//
//  UserDefaults + Extension.swift
//  SoundCloudApp
//
//  Created by Nataly on 25.10.2021.
//

import Foundation

extension UserDefaults {
    static let favoriteTrackKey = "FavoriteTrack"
    
    func getSavedTracksId() -> [Int] {
        object(forKey: UserDefaults.favoriteTrackKey) as? [Int] ?? []
    }
    
    func saveTracks(tracksId: [Int]) {
        set(tracksId, forKey: UserDefaults.favoriteTrackKey)
    }
    
    func isFavorite(trackId: Int) -> Bool {
        let favTracksId = getSavedTracksId()
       return favTracksId.contains(trackId) ? true : false
    }
    
    func remove(trackId: Int) {
        var favTracksId = getSavedTracksId()
        if let index = favTracksId.firstIndex(of: trackId) {
            favTracksId.remove(at: index)
        }
        saveTracks(tracksId: favTracksId)
    }
    
    func add(trackId: Int) {
        var favTracksId = getSavedTracksId()
        favTracksId.append(trackId)
        
        saveTracks(tracksId: favTracksId)
    }
}
