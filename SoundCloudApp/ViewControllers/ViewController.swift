//
//  ViewController.swift
//  SoundCloudApp
//
//  Created by Nataly on 21.09.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInterface()
        addKeyboardObservers()
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if loginTF.text == appUser.getUsersData().email &&
            passwordTF.text == appUser.getUsersData().password {
           
            performSegue(withIdentifier: "showTrackList", sender: nil)
        } else {
            showWrongCredentialsAlert(
                title: "Login failed",
                message: "Your email or password is incorrect. Please, try again."
            )
        }
    }
    
    private func showWrongCredentialsAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    private func setButtonEnabled() {
        if loginTF.hasText, passwordTF.hasText {
            loginButton.isEnabled = true
            loginButton.alpha = 1
        } else {
            loginButton.isEnabled = false
            loginButton.alpha = 0.5
        }
    }
    
    private func setInterface() {
        view.backgroundColor = .black
        
        passwordTF.layer.cornerRadius = 5
        passwordTF.layer.borderWidth = 1
        passwordTF.layer.borderColor = UIColor.white.cgColor
        passwordTF.attributedPlaceholder = NSAttributedString(
            string: "Password",
            attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray]
        )
        passwordTF.isSecureTextEntry = true
        
        loginTF.layer.cornerRadius = 5
        loginTF.layer.borderWidth = 1
        loginTF.layer.borderColor = UIColor.white.cgColor
        loginTF.attributedPlaceholder = NSAttributedString(
            string: "Login",
            attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
        
        loginButton.backgroundColor = #colorLiteral(red: 1, green: 0.3333333333, blue: 0, alpha: 1) //#FF5500
        loginButton.layer.cornerRadius = 5
        loginButton.isEnabled = false
        loginButton.alpha = 0.5
    }
}

// MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        setButtonEnabled()
    }
}
// MARK: - Working with keyboard
extension ViewController {
    
    func addKeyboardObservers() {
        // наблюдатели, отправляющие уведомления о состоянии клавиатуры
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        // размер клавиатуры
        guard let keyboardFrameSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        // увеличиваем вью на высоту клавиатуры
        (self.view as? UIScrollView)?.contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height + keyboardFrameSize.height)
        // ограничиваем дистанцию ползунка до клавиатуры
        (self.view as? UIScrollView)?.scrollIndicatorInsets = UIEdgeInsets(top: .zero, left: .zero, bottom: keyboardFrameSize.height, right: .zero)
        (self.view as? UIScrollView)?.setContentOffset(CGPoint(x: 0, y: keyboardFrameSize.height/2), animated: true)
    }
    
    @objc func keyboardDidHide() {
        // восстанавливаем исходный размер вью
        (self.view as? UIScrollView)?.contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
    }
}


