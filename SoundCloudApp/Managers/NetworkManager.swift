//
//  Network.swift
//  SoundCloudApp
//
//  Created by Nataly on 28.10.2021.
//

import Foundation
import Alamofire
import KeychainSwift

struct Token: Decodable {
    var accessToken: String
    var expiresIn: Int
    var scope: String
    var refreshToken: String
    var tokenType: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case scope = "scope"
        case refreshToken = "refresh_token"
        case tokenType = "token_type"
    }
}

struct RequestParameter: Codable {
    var grantType: String?
    var clientId: String?
    var clientSecret: String?
    var refreshToken: String?
    
    enum CodingKeys: String, CodingKey  {
        case grantType = "grant_type"
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case refreshToken = "refresh_token"
    }
}

class NetworkManager {
    
//    static let shared = NetworkManager()
//    private init() {}
    
    func getAccsessToken() {
        let parameters = RequestParameter(
            grantType: "client_credentials",
            clientId: "08cb4bc9efc7ebeb5945abe37ae11b39",
            clientSecret: "43d7b47398b1e57bf05a6f8ce0cc8a49"
        )
        AF.request("https://api.soundcloud.com/oauth2/token", method: .post, parameters: parameters)
            .validate()
            .responseDecodable(of: Token.self) { dataResponse in
                switch dataResponse.result {
                case .success(let data):
                    KeychainManager.shared.save(data.accessToken, forKey: "accessToken")
                    KeychainManager.shared.save(data.refreshToken, forKey: "refreshToken")
                    
                    Timer.scheduledTimer(timeInterval: Double(data.expiresIn), target: self, selector: #selector(self.refreshAccsessToken), userInfo: nil, repeats: true)
                    
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    @objc func refreshAccsessToken() {
        guard let refreshToken = KeychainSwift().get("refreshToken") else { return }
        
        let parameters = RequestParameter(
            grantType: "refresh_token",
            clientId: "08cb4bc9efc7ebeb5945abe37ae11b39",
            clientSecret: "43d7b47398b1e57bf05a6f8ce0cc8a49",
            refreshToken: "\(refreshToken)"
        )
        AF.request("https://api.soundcloud.com/oauth2/token", method: .post, parameters: parameters)
            .validate()
            .responseDecodable(of: Token.self) { dataResponse in
                switch dataResponse.result {
                case .success(let data):
                    //print(data)
                    KeychainManager.shared.save(data.accessToken, forKey: "accessToken")
                    KeychainManager.shared.save(data.refreshToken, forKey: "refreshToken")
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    func fetchTracks(query: String, completion: @escaping ([Track]) -> ()) {
        
        guard let url = URL(string: "https://api.soundcloud.com/tracks") else { return }
        let parameters = [
            "q" : "\(query)",
            "access" : "playable",
            "limit" : "3",
            "linked_partitioning" : "true"
        ]
        
        guard let accessToken = KeychainSwift().get("accessToken") else { return }
        let headers: HTTPHeaders = [
            .authorization("OAuth \(accessToken)"),
            .accept("application/json; charset=utf-8")
        ]
        AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            .validate()
            .responseDecodable(of: TrackList.self) { dataResponse in
                switch dataResponse.result {
                case .success(let trackList):
                    guard let tracks = trackList.collection else { return }
                    completion(tracks)
                case .failure(let error):
                    print(error)
                }
            }
    }
    
}

