//
//  ImageManager.swift
//  SoundCloudApp
//
//  Created by Nataly on 06.10.2021.
//

import Foundation
import UIKit

class ImageManager {
    static let shared = ImageManager()
    private init() {}
    
    func fetchImage(from string: String) -> UIImage? {
        guard
            let url = URL(string: string),
            let data = try? Data(contentsOf: url),
            let image = UIImage(data: data)
        else {
            return UIImage(named: "noImage")
        }
        return image
    }
}
