//
//  MaxiTrackViewController.swift
//  SoundCloudApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit

class MaxiTrackViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
//    это окрашивается в белый цвет, чтобы скрыть фон.
//    Он не имеет высоты, поэтому не влияет на содержимое scrollview
    @IBOutlet weak var stretchySkirt: UIView!
    
    
    @IBOutlet weak var coverImageContainer: UIView!
    @IBOutlet weak var coverArtImage: UIImageView!
    @IBOutlet weak var dismissButtton: UIButton!
    
    var backingImage: UIImage?
    @IBOutlet weak var backingImageView: UIImageView!
    @IBOutlet weak var dimmerLayer: UIView!
    
    @IBOutlet weak var backingImageLeadingInset: NSLayoutConstraint!
    @IBOutlet weak var backingImageBottomInset: NSLayoutConstraint!
    @IBOutlet weak var backingImageTopInset: NSLayoutConstraint!
    @IBOutlet weak var backingImageTrailingInset: NSLayoutConstraint!
    
    
    var currentSong: Track?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
