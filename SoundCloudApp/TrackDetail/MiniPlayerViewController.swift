//
//  MiniPlayerViewController.swift
//  SoundCloudApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit


protocol MiniPlayerDelegate: AnyObject {
  func expandSong(song: Track)
}

class MiniPlayerViewController: UIViewController {

    weak var delegate: MiniPlayerDelegate?
    var currentSong: Track?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MiniPlayerViewController {

    @IBAction func tapGest(_ sender: Any) {
        print(#function)
        guard let song = currentSong else {
          return
        }

        delegate?.expandSong(song: song)
    }
    
    
    
}
