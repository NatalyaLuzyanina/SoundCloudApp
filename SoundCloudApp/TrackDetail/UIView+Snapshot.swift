//
//  UIView+Snapshot.swift
//  SoundCloudApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit

extension UIView  {

  func makeSnapshot() -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
    drawHierarchy(in: bounds, afterScreenUpdates: true)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
}
