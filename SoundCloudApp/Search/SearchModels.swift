//
//  SearchModels.swift
//  SoundCloudApp
//
//  Created by Nataly on 02.11.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

enum Search {
    
    enum Model {
        struct Request {
            enum RequestType {
                case getTracks(searchTerm: String)
                case getTokens
                case setFavorite(cell: SearchViewModel.Cell)
            }
        }
        struct Response {
            enum ResponseType {
                case presentTracks(tracks: [Track])
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayTracks(searchViewModel: SearchViewModel)
            }
        }
    }
}

struct SearchViewModel {
    struct Cell {
        var iconUrl: String
        var title: String
        var artistName: String
        var trackId: Int
        
        func isFavorite() -> Bool {
            UserDefaults.standard.isFavorite(trackId: trackId)
        }
    }
    
    let cells: [Cell]
}
