//
//  SearchViewController.swift
//  SoundCloudApp
//
//  Created by Nataly on 02.11.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchDisplayLogic: AnyObject {
  func displayData(viewModel: Search.Model.ViewModel.ViewModelData)
}

class SearchViewController: UIViewController, SearchDisplayLogic {

  var interactor: SearchBusinessLogic?
  var router: (NSObjectProtocol & SearchRoutingLogic)?

  // MARK: Object lifecycle
  
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coverimage: UIImageView!
    @IBOutlet weak var typeSmthLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var searchViewModel = SearchViewModel(cells: [])
    private var timer: Timer?
  
    
  // MARK: Setup
  
  private func setup() {
    let viewController        = self
    let interactor            = SearchInteractor()
    let presenter             = SearchPresenter()
    let router                = SearchRouter()
    viewController.interactor = interactor
    viewController.router     = router
    interactor.presenter      = presenter
    presenter.viewController  = viewController
    router.viewController     = viewController
  }
  
  // MARK: Routing
  

  
  // MARK: View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setup()
    setupSearchBar()
    interactor?.makeRequest(request: Search.Model.Request.RequestType.getTokens)
  }
  
    private func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func displayData(viewModel: Search.Model.ViewModel.ViewModelData) {
        switch viewModel {
        case .displayTracks(let searchViewModel):
            self.searchViewModel = searchViewModel
            tableView.reloadData()
            self.activityIndicator.stopAnimating()
            self.coverimage.isHidden = true
            self.typeSmthLabel.isHidden = true
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchViewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as? TrackCell else { return UITableViewCell() }
        
        let viewModelCell = searchViewModel.cells[indexPath.row]
        cell.link = self
        cell.configure(searchViewModelCell: viewModelCell)
        return cell
    }
}

extension SearchViewController: UISearchResultsUpdating, UISearchBarDelegate {
   
   func updateSearchResults(for searchController: UISearchController) {
    guard let query = searchController.searchBar.text else { return }
    
    timer?.invalidate()
    if query.isEmpty {
        coverimage.isHidden = false
        typeSmthLabel.isHidden = false
    } else {
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { _ in
            print(query)
            self.typeSmthLabel.isHidden = true
            self.activityIndicator.startAnimating()
            self.interactor?.makeRequest(request: Search.Model.Request.RequestType.getTracks(searchTerm: query))
        })
    }
    }
}

extension SearchViewController: MiniPlayerDelegate {
  func expandSong(song: Track) {
    //1.
    guard let maxiCard = storyboard?.instantiateViewController(
              withIdentifier: "MaxiSongCardViewController")
              as? MaxiTrackViewController else {
      assertionFailure("No view controller ID MaxiSongCardViewController in storyboard")
      return
    }
    
    //2.
    maxiCard.backingImage = view.makeSnapshot()
    //3.
    maxiCard.currentSong = song
    //4.
    present(maxiCard, animated: false)
  }
}
