//
//  SearchPresenter.swift
//  SoundCloudApp
//
//  Created by Nataly on 02.11.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchPresentationLogic {
    func presentData(response: Search.Model.Response.ResponseType)
}

class SearchPresenter: SearchPresentationLogic {
    weak var viewController: SearchDisplayLogic?
    
    func presentData(response: Search.Model.Response.ResponseType) {
        switch response {
        case .presentTracks(let tracks):
            let cells = tracks.map { track in
                createCellViewModel(track: track)
            }
            let searchViewModel = SearchViewModel.init(cells: cells)
            
            viewController?.displayData(viewModel: Search.Model.ViewModel.ViewModelData.displayTracks(searchViewModel: searchViewModel))
        }
    }
    
    func createCellViewModel(track: Track) -> SearchViewModel.Cell {
        SearchViewModel.Cell.init(iconUrl: track.image ?? "",
                                  title: track.title ?? "",
                                  artistName: track.user?.username ?? "",
                                  trackId: track.id ?? 0)
    }
    
}

