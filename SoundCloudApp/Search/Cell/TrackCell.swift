//
//  TrackCell.swift
//  SoundCloudApp
//
//  Created by Nataly on 03.11.2021.
//

import UIKit
import SDWebImage

class TrackCell: UITableViewCell {

    private var searchViewModelCell: SearchViewModel.Cell?
    var link: SearchViewController?
    
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var trackTitleLabel: UILabel!
    @IBOutlet weak var trackAuthorLabel: UILabel!
    @IBOutlet weak var trackImage: UIImageView!

    override func prepareForReuse() {
        super.prepareForReuse()
        trackImage.image = nil
    }
    
    @IBAction func setFavoriteTrack(_ sender: Any) {
        starButton.tintColor = (searchViewModelCell?.isFavorite() ?? false) ? .systemGray5  : .yellow
        
        guard let searchViewModelCell = searchViewModelCell else { return }
        link?.interactor?.makeRequest(request: Search.Model.Request.RequestType.setFavorite(cell: searchViewModelCell))
    }
    
    func configure(searchViewModelCell: SearchViewModel.Cell) {
        
        trackTitleLabel.text = searchViewModelCell.title
        trackAuthorLabel.text = searchViewModelCell.artistName
        starButton.tintColor = searchViewModelCell.isFavorite() ? .yellow : .systemGray5
        
        guard let url = URL(string: searchViewModelCell.iconUrl) else { return }
        trackImage.sd_setImage(with: url, completed: nil)
        
        self.searchViewModelCell = searchViewModelCell
    }
}
