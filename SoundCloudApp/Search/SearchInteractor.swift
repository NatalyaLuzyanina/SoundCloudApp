//
//  SearchInteractor.swift
//  SoundCloudApp
//
//  Created by Nataly on 02.11.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchBusinessLogic {
    func makeRequest(request: Search.Model.Request.RequestType)
}

class SearchInteractor: SearchBusinessLogic {
    
    var networkService = NetworkManager()
    var presenter: SearchPresentationLogic?
    var service: SearchService?
    
    func makeRequest(request: Search.Model.Request.RequestType) {
        if service == nil {
            service = SearchService()
        }
        
        switch request {
        case .getTracks(let searchTerm):
            networkService.fetchTracks(query: searchTerm) { [weak self] tracks in
                DispatchQueue.main.async {
                    self?.presenter?.presentData(response: Search.Model.Response.ResponseType.presentTracks(tracks: tracks))
                }
            }
        case .getTokens:
            networkService.getAccsessToken()
        case .setFavorite(let track):
            if track.isFavorite() {
                UserDefaults.standard.remove(trackId: track.trackId)
            } else {
                UserDefaults.standard.add(trackId: track.trackId)
            }
        }
    }
}
